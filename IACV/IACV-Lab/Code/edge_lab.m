%Edge detection
clc;
clear all;
close all;
pkg load image;

f = imread('Cameraman.jpg');
lena = imread("Lena.jpg");
gboth = edge(f,'Canny'); %%vertical and horizontal
gboth2 = edge(lena,'Canny');
g1 = edge(f, 'Canny', 0.15);
g2 = edge(lena, 'Canny', 0.1);

figure;
subplot(121),imshow(gboth),title('Canny Magnitude',"fontsize",15);
subplot(122),imshow(gboth2),title('Canny Magnitude',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);
figure;
subplot(121),imshow(g1),title('Final Merged Result with T=0.15',"fontsize",15);
subplot(122),imshow(g2),title('Final Merged Result with T=0.1',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);

perwittx= [-3 ,0 ,3; -3,0,3; -3, 0 ,3];
perwity= [-3,-3,-3; 0, 0, 0; 3, 3,3];
H = conv2(im2double(f),perwittx,'same');
V = conv2(im2double(f),perwity,'same');
E = sqrt(H.*H + V.*V); % or sqrt(H.�2+V.�2)
grad=atan2(V,H);

g1 = edge(lena, 'Prewitt', 0.15);
g2 = edge(f, 'Prewitt', 0.1);

figure
subplot(221),imshow(H),title('Vertical Perwitt', "fontsize",15);
subplot(222),imshow(V),title('Horizontal Perwitt', "fontsize", 15);
subplot(223),imshow(grad),title('Gradient', "fontsize", 15);
subplot(224),imshow(E, []), title('Magnitude', "fontsize",15);
S  = axes( 'visible', 'off', 'title', 'Perwitt Edge Detection' , 'fontsize', 15);
figure;
subplot(121),imshow(g1),title('Final Merged Result with T=0.15',"fontsize",15);
subplot(122),imshow(g2),title('Final Merged Result with T=0.1',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'Perwitt Edge Detection - Thresholded' , 'fontsize', 15);


gboth = edge(f,'LoG'); %%vertical and horizontal
gboth2 = edge(lena,'LoG');
g1 = edge(f, 'LoG', 0.15);
g2 = edge(lena, 'LoG', 0.1);

figure;
subplot(121),imshow(gboth),title('LoG Magnitude',"fontsize",15);
subplot(122),imshow(gboth2),title('LoG Magnitude',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);
figure;
subplot(121),imshow(g1),title('LoG Merged Result with T=0.15',"fontsize",15);
subplot(122),imshow(g2),title('LoG Merged Result with T=0.1',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);
