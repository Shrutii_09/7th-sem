clc;
clear all;
pkg load signal;
pkg load image;
A= imread('Cameraman.jpg');
B=dct2(A);
C=uint8(idct2(B));

%% without using inbuilt
N = size(A)(1);
T = zeros(N,N);
 
for i=1:N
  for j=1:N
	if i==1
  	T(i,j) = 1/sqrt(N);
	else
  	T(i,j) = sqrt(2/N)*cos(((2*(j-1)+1)*(i-1)*3.14)/(2*N));
	endif
  endfor
endfor
 
dctimg = T*double(A)*inv(T);
idctimg = inv(T)*dctimg*T;


figure;
subplot(2,2,1);
imshow(A)
title('Original image','fontsize',15) ;

subplot(2,2,2);
imshow(B)
title('DCT of Cameraman', 'fontsize',15);

subplot(2,2,3);
imshow(dctimg)
title('DCT of Cameraman without inbuilt', 'fontsize',15);

subplot(2,2,4) ;
imshow(C) 
title("IDCT of DCT Cameraman image",'fontsize',15);

S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);
