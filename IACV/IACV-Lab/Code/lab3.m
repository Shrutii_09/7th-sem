clc;
clear all;
close all;
pkg load image;
a = [];  %matrix to store positions where X=3 apears in Lena
b = []; %matrix to store positions where X=3 apears in Cameraman

Lena = imread('Lena.jpg');
Cameraman = imread('Cameraman.jpg');
n_a = 0; %number of times X appears in I
n_b = 0; %number of times X appears in J 
X = 99
mod_cam=zeros(size(Cameraman));
mod_lena=zeros(size(Lena));
for i = 1:512
  for j = 1:512
    if Lena(i,j) == X
      a = [a;i,j];
      mod_lena(i,j)=255;
      n_a = n_a +1;
    endif
    if Cameraman(i,j) == X
      b = [b;i,j];
      mod_cam(i,j)=255;
      n_b = n_b +1;
    endif
  endfor
endfor

disp('Count of X in Lena')
disp(n_a)
##disp('Count of X in Cameraman')
##disp(n_b)
%disp('X in Lena')
%disp(a)
%disp('X in Cameraman')
%disp(b(484:644,:))

##subplot(1,2,1);
##imshow(mod_cam)
##title('Intensity Search in Cameraman','fontsize',20) ;
##subplot(1,2,2) ;
##imshow(mod_lena) 
##title('Intensity search in Lena','fontsize',20);
##S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);