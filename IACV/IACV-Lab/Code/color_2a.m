clc;
close all;
clear all;
pkg load image;

%Function for Threshold 1
function y = threshold1(x)
x=double(x);
[m,n,c] = size(x);
y = zeros(m,n,c);
y(x>127) = 255;
end

%Function for Threshold 2
function y = threshold2(x)
x=double(x);
[m,n,c] = size(x);
y = 255*ones(m,n,c);
y(x<128)=x(x<128);
endfunction

%Function for Threshold 3
function y = threshold3(x)
x=double(x);
[m,n,c] = size(x);
y = 255*ones(m,n,c);
y(x<=127)=-((128/127)*x(x<=127))+255;
endfunction
 
 
%Function for Threshold 4
function y = threshold4(x)
x=double(x);
[m,n,c] = size(x);
y = 255*ones(m,n,c);
y(x>127)=fix((-127/128)*(x(x>127)-255));
endfunction

 
%Function for Threshold 5
function y = threshold5(x)
x=double(x);
[m,n,c] = size(x);
y = 255*ones(m,n,c);
y(x>126) = x(x>126);
end

%Function for Threshold 6
function y = threshold6(x)
x=double(x);
[m,n,c] = size(x);
y = 255*ones(m,n,c);
y(127<x & x<201) = fix(((155/73)*(x(127<x & x<201)-200))+100);
y(x>200) = 100;
end

%Function for Threshold 7
function y = threshold7(x)
x=double(x);
[m,n,c] = size(x);
y = 255*ones(m,n,c);
y(x<=175) = fix((200/175)*x(x<=175));
end

%Function for Threshold 8
function y = threshold8(x)
x=double(x);
[m,n,c] = size(x);
y = 100*ones(m,n,c);
y(x<=175) = fix(((-155/175)*x(x<=175))+255); 
end

%Function for Threshold 9
function y = threshold9(x)
x=double(x);
[m,n,c] = size(x);
y = zeros(m,n,c);
y(x>50&x<126)=255;
y(x>200) = 255;
end


%Function for Threshold 10
function y = threshold10(x)
x=double(x);
[m,n,c] = size(x);
y = zeros(m,n,c);
y(50<x & x<88) =  fix((255/37)*(x(50<x & x<88)-50)); 
y(87<x & x<126) =fix(((-255/38)*x(87<x & x<126))+125);
y(x>201) = fix(((-128/55)*(x(x>201)-255))+200);
end

%Function for Threshold 11
function y = threshold11(x)
  x=double(x);
[m,n,c] = size(x);
y = zeros(m,n,c);
y(x<65) = fix((255/64)*x(x<65));
y(64<x & x<88) =  fix((-255/63)*(x(64<x & x<88)-127));  
y(87<x & x<193) = fix((255/65)*(x(87<x & x<193)-127));
y(x>192) = fix((-255/63)*(x(x>192)-255));
end

%Function for Threshold 12
function y = threshold12(x)
  x=double(x);
[m,n,c] = size(x);
y = zeros(m,n,c);
y(x<65) = fix((-255/64)*(x(x<65)-64));
y(64<x & x<128) = fix((255/63)*(x(64<x & x<128)-64));  
y(127<x & x<193) =fix((-255/65)*(x(127<x & x<193)-192));
y(x>192) = fix((255/63)*(x(x>192)-192));
end

%Function for Threshold 13
function y = threshold13(x)
  x=double(x);
[m,n,c] = size(x);
y = 255*ones(m,n,c);
y(64<x & x<128) = fix((-255/63)*(x(64<x& x<128)-127));    
y(127<x& x<193) = fix((255/65)*(x(127<x& x<193)-127));
end


%Function for Threshold 14
function y = threshold14(x)
  x=double(x);
[m,n,c] = size(x);
y = 200*ones(m,n,c);
y(x<51) = fix(((-55/50)*x(x<51))+255); 
y(50<x & x<101) = fix(2*(x(50<x & x<101)-50)+100);    
y(150<x & x<176) = fix(-4*(x(150<x & x<176)-150) + 200);
y(175<x & x<226) = fix(2*(x(175<x & x<226)-175) + 100); 
end


%Function for Threshold 15
function y = threshold15(x)
  x=double(x);
[m,n,c] = size(x);
y = 60*ones(m,n,c);
y(x<76) = fix(((60/75)*x(x<76)));
y(75<x & x<121) = fix(((195/45)*(x(75<x & x<121)-75))+60);  
y(120<x & x<176) = 192;
y(175<x & x<201) =  fix(((-67/25)*(x(175<x & x<201)-200)) + 60);
y(x>225)=fix(-1*(x(x>225) -255));
end


%Function for Threshold 16
function y = threshold16(x)
  x=double(x);
[m,n,c] = size(x);
y = 255*ones(m,n,c);
y(75<x & x<151) = fix((-255/75)*(x(75<x & x<151)-150)); 
y(150<x & x<201) = fix((255/50)*(x(150<x& x<201)-150));  
end


%Taking Input
img_pepper = imread('pepper.jpg');


%Applying Threshold
tic
img_pepper_t1 = uint8(threshold1(img_pepper));

img_pepper_t2 = uint8(threshold2(img_pepper));

img_pepper_t3 = uint8(threshold3(img_pepper));

img_pepper_t4 = uint8(threshold4(img_pepper));

img_pepper_t5 = uint8(threshold5(img_pepper));

img_pepper_t6 = uint8(threshold6(img_pepper));

img_pepper_t7 = uint8(threshold7(img_pepper));

img_pepper_t8 = uint8(threshold8(img_pepper));

img_pepper_t9 = uint8(threshold9(img_pepper));

img_pepper_t10 = uint8(threshold10(img_pepper));

img_pepper_t11 = uint8(threshold11(img_pepper));

img_pepper_t12 = uint8(threshold12(img_pepper));

img_pepper_t13 = uint8(threshold13(img_pepper));

img_pepper_t14 = uint8(threshold14(img_pepper));

img_pepper_t15 = uint8(threshold15(img_pepper));

img_pepper_t16 = uint8(threshold16(img_pepper));



toc
figure;
subplot(2,2,1)
imshow(img_pepper_t1);
title('Threshold 1', 'fontsize', 15);

subplot(2,2,2)
imshow(img_pepper_t2);
title('Threshold 2', 'fontsize', 15);

subplot(2,2,3)
imshow(img_pepper_t3);
title('Threshold 3', 'fontsize', 15);

subplot(2,2,4)
imshow(img_pepper_t4);
title('Threshold 4', 'fontsize', 15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);


figure;
subplot(2,2,1)
imshow(img_pepper_t5);
title('Threshold 5', 'fontsize', 15);

subplot(2,2,2)
imshow(img_pepper_t6);
title('Threshold 6', 'fontsize', 15);

subplot(2,2,3)
imshow(img_pepper_t7);
title('Threshold 7', 'fontsize', 15);

subplot(2,2,4)
imshow(img_pepper_t8);
title('Threshold 8', 'fontsize', 15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);


figure;
subplot(2,2,1)
imshow(img_pepper_t9);
title('Threshold 9', 'fontsize', 15);

subplot(2,2,2)
imshow(img_pepper_t10);
title('Threshold 10', 'fontsize', 15);

subplot(2,2,3)
imshow(img_pepper_t11);
title('Threshold 11', 'fontsize', 15);

subplot(2,2,4)
imshow(img_pepper_t12);
title('Threshold 12', 'fontsize', 15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);


figure;
subplot(2,2,1)
imshow(img_pepper_t13);
title('Threshold 13', 'fontsize', 15);

subplot(2,2,2)
imshow(img_pepper_t14);
title('Threshold 14', 'fontsize', 15);

subplot(2,2,3)
imshow(img_pepper_t15);
title('Threshold 15', 'fontsize', 15);

subplot(2,2,4)
imshow(img_pepper_t16);
title('Threshold 16', 'fontsize', 15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);


