clc;
close all;


clc;
close all;

lena = imread('Lena.jpg');
cm = imread('Cameraman.jpg');
[r,c]=size(lena);
tic
for i=1:r
  for j = 1:c
    if lena(i,j) > 105
      lena(i,j) = 255;
     else
      lena(i,j) = 0;
    endif
    if cm(i,j) > 85
      cm(i,j) = 255;
    else
      cm(i,j) = 0;
    endif
  endfor
endfor
toc
subplot(1,2,1);
imshow(lena);
title('Thresholding using histogram',"fontsize",20);
subplot(1,2,2);
imshow(cm);
title('Thresholding using histogram',"fontsize",20)