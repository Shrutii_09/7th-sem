clc;
close all;
clear all;

pkg load image;

I=imread('Lena.jpg');
J=imread('Cameraman.jpg');

I1=I+99; % Adding constant k=99
J1=J+99;
I1 = min(I1, 255); I1 = max(I1, 0);   % limiting values to 0 and 255
J1 = min(J1, 255); J1 = max(J1, 0);
figure;
subplot(1,2,1);
imshow(I1)
title('Adding Constant to Lena','fontsize',20) ;
subplot(1,2,2) ;
imshow(J1) 
title('Adding Constant to Cameraman','fontsize',20);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);

I1=I-99; % Subtracting constant k=99
J1=J-99;
I1 = min(I1, 255); I1 = max(I1, 0);   % limiting values to 0 and 255
J1 = min(J1, 255); J1 = max(J1, 0);
figure;
subplot(1,2,1);
imshow(I1)
title('Subtracting Constant to Lena','fontsize',20) ;
subplot(1,2,2) ;
imshow(J1) 
title('Subtracting Constant to Cameraman','fontsize',20);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);

I1=I*2; % Multiplying constant k=2
J1=J*2;
I1 = min(I1, 255); I1 = max(I1, 0);   % limiting values to 0 and 255
J1 = min(J1, 255); J1 = max(J1, 0);
figure;
subplot(1,2,1);
imshow(I1)
title(' Multiplying Constant to Lena','fontsize',20) ;
subplot(1,2,2) ;
imshow(J1) 
title(' Multiplying Constant to Cameraman','fontsize',20);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);

I1=round(I/2); % Dividing constant k=2
J1=round(J/2);
I1 = min(I1, 255); I1 = max(I1, 0);   % limiting values to 0 and 255
J1 = min(J1, 255); J1 = max(J1, 0);
figure;
subplot(1,2,1);
imshow(I1)
title(' Dividing Constant to Lena','fontsize',20) ;
subplot(1,2,2) ;
imshow(J1) 
title(' Dividing Constant to Cameraman','fontsize',20);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);

I2 = imadd(I, J);
figure; imshow(I2)
title('Lena + Cameraman','fontsize',20) ;

I2 = imsubtract(I, J);
J2 = imsubtract(J, I);
figure;
subplot(1,2,1);
imshow(I2)
title('Lena - Cameraman ','fontsize',20) ;
subplot(1,2,2) ;
imshow(J2) 
title('Cameraman - Lena','fontsize',20);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);

I3=I.*J;
I3 = min(I3, 255); I3 = max(I3, 0);   % limiting values to 0 and 255
figure; imshow(I3)
title('Lena * Cameraman','fontsize',20);

I3=round(I./J);
J3 = round(J./I);
I3 = min(I3, 255); I3 = max(I3, 0);   % limiting values to 0 and 255
J3 = min(J3, 255); J3 = max(J3, 0);   % limiting values to 0 and 255
figure; subplot(1,2,1);
imshow(I3)
title('Lena / Cameraman','fontsize',20) ;
subplot(1,2,2);
imshow(J3)
title('Cameraman / Lena','fontsize',20) ;
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);