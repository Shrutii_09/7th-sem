%Edge detection
clc;
clear all;
close all;
pkg load image;
function [ output ] = self_conv( input,kernel )
  [rows,cols] = size(input); %// Change

  %// New - Create a padded matrix that is the same class as the input
  new_img = zeros(rows+2,cols+2);
  new_img = cast(new_img, class(input));

  %// New -  Place original image in padded result
  new_img(2:end-1,2:end-1) = input;

  %// Also create new output image the same size as the padded result
  output = zeros(size(new_img));
  output = cast(output, class(input));

  for i=2:1:rows+1 %// Change
    for j=2:1:cols+1 %// Change
      value=0;
      for g=-1:1:1
        for l=-1:1:1
          value=value+new_img(i+g,j+l)*kernel(g+2,l+2); %// Change
        endfor
      endfor
     output(i,j)=value;
    endfor
  endfor

%// Change
%// Crop the image and remove the extra border pixels
output = output(2:end-1,2:end-1);
endfunction

my_img=imread('Lena.jpg') ;
my_img2=imread('Cameraman.jpg') ;
kx= [1 ,0 ,-1; 2,0,-2; 1, 0 ,-1];
ky= [1,2,1; 0,0, 0; -1, -2 ,-1];

H = conv2(im2double(my_img),kx,'same');
V = conv2(im2double(my_img),ky,'same');
E = sqrt(H.*H + V.*V); % or sqrt(H.^2+V.^2)
grad = atan2(V,H);
figure;
subplot(221),imshow(H),title('Horizontal Sobel',"fontsize",15);
subplot(222),imshow(V),title('Vertical Sobel',"fontsize",15);
subplot(223),imshow(grad),title('Gradient',"fontsize",15);
subplot(224),imshow(E),title('Magnitude',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'Sobel Edge Detection' , 'fontsize', 15);

g1 = edge(my_img,'Sobel',0.1);
g2 = edge(my_img2,'Sobel',0.1);
figure;
subplot(121),imshow(g1),title('Threshold - 0.1',"fontsize",15);
subplot(122),imshow(g2),title('Threshold - 0.1',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'Sobel Edge Detection - Thrresholded' , 'fontsize', 15);