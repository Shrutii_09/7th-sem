## Line Detection
clc;
clear all;
close all;
pkg load image;

f = imread('wire.png');
f = rgb2gray(f);
w=[2,-1,-1;-1,2,-1;-1,-1,2];   %%-45
g1=conv2(im2double(f),w,'same');

wh = [-1,-1,-1;2,2,2;-1,-1,-1];   %%horizontal
g2 = conv2(im2double(f),wh,'same');

wv = [-1,2,-1;-1,2,-1;-1,2,-1];   %%horizontal
g3 = conv2(im2double(f),wv,'same');

w2 = [-1,-1,2;-1,2,-1;2,-1,-1];   %% 45
g4 = conv2(im2double(f),w2,'same');
figure;
subplot(221),imshow(g2),title('horizontal detector',"fontsize",15);
subplot(222),imshow(g1),title(' -45 detector',"fontsize",15);
subplot(223),imshow(im2double(g3)),title(' vertical detector',"fontsize",15);
subplot(224),imshow(im2double(g4)),title(' 45 detector',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);
%combined 
O = max(g1,g2);
C = max(O,g3);
y = max(C,g4);
figure;
subplot(121),imshow(f),title('Original Image','fontsize',15);
subplot(122),imshow(im2double(y)),title('Final Merged Result',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);
T=max(y(:));
gg=y>=T;
s=y>=10;
p=y>=215;
figure;

subplot(131),imshow(s),title('Threshold 99',"fontsize",15);
subplot(132),imshow(gg),title('Max(y) Threshold',"fontsize",15);
subplot(133),imshow(p),title('Threshold 215',"fontsize",15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);