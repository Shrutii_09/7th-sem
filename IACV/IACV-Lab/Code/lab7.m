clc;
clear all;
close all;
pkg load image;
img = imread('wire.png');
img = rgb2gray(img);
se = [1 1 1; 1 1 1 ; 1 1 1];
d = imdilate(img, se);
e = imerode(img , se);
figure;
s3=subplot(1,3,1)
imshow(img);
set(s3,'title','Original Image','fontsize', 15 );
s1=subplot(1,3,2)
imshow(d);
set(s1,'title','Dilated Image ','fontsize', 15 );
s2= subplot(1,3,3)
imshow(e);
set(s2,'title','Eroded Image ','fontsize', 15 );
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 16);