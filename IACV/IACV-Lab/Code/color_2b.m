clc;
close all;
clear all;
pkg load image;

%Function for Threshold red
function y = threshold_red(x)
[m,n] = size(x);
y = zeros(m,n);
y(50<x& x<88)=fix((255/37)*(x(50<x & x<88)-50));
y(87<x & x<126) = fix((-255/38)*(x(87<x & x<126)-125));
y(x>200)=fix(((-128/55)*(x(x>200)-200))+255);
end

%Function for Threshold green
function y = threshold_green(x)
[m,n] = size(x);
y = zeros(m,n);
y(x<65) = fix((-255/64)*(x(x<65)-64));
y(64<x & x<128) = fix((255/63)*(x(64<x & x<128)-64));
y(127<x & x<193) = fix((-255/65)*(x(127<x & x<193)-192));
y(x>192) = fix((255/63)*(x(x>192)-192));
end

%Function for Threshold blue 
function y = threshold_blue(x)
[m,n] = size(x);
y = 60*ones(m,n);
y(x<76) = fix((60/75)*x(x<76));
y(75<x&x<121) = fix(((195/45)*(x(75<x & x<121)-75))+60);  
y(120<x & x<176) = 192;
y(175<x & x<201) =  fix(((-67/25)*(x(175<x & x<201)-200)) + 60);
y(x>225)=fix(-1*(x(x>225) -255));
end

%Taking Input
img_panda = imread('pepper.jpg');
%img_panda = imnoise(img_panda,'salt & pepper');
[m,n,c] = size(img_panda);
op = zeros(m,n,c);
op(:,:,1) = uint8(threshold_red(img_panda(:,:,1)));
op(:,:,2) = uint8(threshold_green(img_panda(:,:,2)));
op(:,:,3) = uint8(threshold_blue(img_panda(:,:,3)));

figure;
subplot(1,2,1);
imshow(img_panda(:,:,1));
title('Red Plane','fontsize',15);
subplot(1,2,2);
imshow(op(:,:,1));
title('Red Thresholded Plane','fontsize',15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);

figure;
subplot(1,2,1);
imshow(img_panda(:,:,2));
title('Green Plane','fontsize',15);
subplot(1,2,2);
imshow(op(:,:,2));
title('Green Thresholded Plane','fontsize',15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);

figure;
subplot(1,2,1);
imshow(img_panda(:,:,3));
title('Blue Plane','fontsize',15);
subplot(1,2,2);
imshow(op(:,:,3));
title('Blue Thresholded Plane','fontsize',15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);

figure;
subplot(1,2,1);
imshow(img_panda);
title('Original Image','fontsize',15);
subplot(1,2,2);
imshow(op);
title('Thresholded Image','fontsize',15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);