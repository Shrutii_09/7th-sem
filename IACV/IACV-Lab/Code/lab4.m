clc;
clear all;
close all;
pkg load image;

I = imread('Lena.jpg');
J = imread('Cameraman.jpg');
H1=histeq(I);
figure;
s1 = subplot(2,2,1)
imshow(I); set( s1, 'title', 'Original Lena Image','fontsize',15 );
s2 = subplot(2,2,2)
imshow(H1); set( s2, 'title', 'Equalized Image','fontsize', 15);
s3 = subplot(2,2,3)
imhist(I); set( s3, 'title', 'Histogram of original Lena','fontsize', 15 );
s4 = subplot(2,2,4)
imhist(H1); set(s4,'title','Histogram of equalized Lena','fontsize', 15 );
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 16);

H2=histeq(J);
figure;
s1 =subplot(2,2,1)
imshow(J);set( s1, 'title', 'Original Cameraman Image','fontsize',15 );
s2=subplot(2,2,2)
imshow(H2); set( s2, 'title', 'Equalized Image','fontsize', 15);
s3=subplot(2,2,3)
imhist(J); set( s3, 'title', 'Histogram of original Cameraman','fontsize', 15 );
s4=subplot(2,2,4)
imhist(H2); 
set(s4,'title','Histogram of equalized Cameraman','fontsize', 15 );
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 14);