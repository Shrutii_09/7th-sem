%understanding basics of images and 2d signal handling by generation of various patterns 
clc; 
clear all; 
close all; 

function im = display_borders(im)
im(:, 1:3)=0;
im(1:3, :)=0;
im(:, 254:256)=0;
im(254:256, :)=0;
end

%pattern 1 
im1=zeros(256,256); 
im1 = display_borders(im1);

%pattern 2 
im2=ones(256,256); 
im2 = display_borders(im2);

%pattern 3 
im3=im1;
im3(:, 1:128)=1;
im3 = display_borders(im3);

%pattern 4 
im4 = abs(1 - im3);
im4 = display_borders(im4);

%pattern 5 
im5=im1;
im5(128:end, :)=1;
im5 = display_borders(im5);

%pattern 6 
im6 = abs(1 - im5);
im6 = display_borders(im6);

%pattern 7
im7 =im1;
im7(1:128, 128:256)=1;
im7(129:256, 1:128)=1;
im7 = display_borders(im7);

%pattern 8
im8 = abs(1-im7);
im8 = display_borders(im8);

%pattern 9 chess 
im9=checkerboard(32);
im9 = (checkerboard > 0.5); 

%showing the patterns (imshow) 
figure 
subplot(1,2,1) 
imshow(im2) 
title('pattern 1','fontsize',20) 
subplot(1,2,2) 
imshow(im1) 
title('pattern 2','fontsize',20) 
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);
figure
subplot(1,2,1) 
imshow(im3) 
title('pattern 3','fontsize',20) 
subplot(1,2,2) 
imshow(im4) 
title('pattern 4','fontsize',20)
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);
figure
subplot(1,2,1) 
imshow(im5) 
title('pattern 5','fontsize',20)
subplot(1,2,2)
imshow(im6) 
title('pattern 6','fontsize',20)
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);
figure
subplot(1,2,1)
imshow(im7) 
title('pattern 7','fontsize',20)
subplot(1,2,2)
imshow(im8) 
title('pattern 8','fontsize',20)
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 20);
figure
title('pattern 9','fontsize',20)
