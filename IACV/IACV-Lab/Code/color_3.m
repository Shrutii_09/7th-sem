clear;
clc;
pkg load image
img=imread("pepper.jpg");
figure, imshow(img);
title("Original Image",'fontsize',15);

function out=RGB_to_XYZ(img);
  img_size=size(img);
  out=zeros(img_size);
  R=img(:,:,1);
  G=img(:,:,2);
  B=img(:,:,3);
  
  out(:,:,1) = 0.4124564*R + 0.3575761*G + 0.1804375*B;
  out(:,:,2) = 0.2126729*R + 0.7151522*G + 0.0721750*B;
  out(:,:,3) = 0.0193339*R + 0.1191920*G + 0.9503041*B;
  
endfunction


function out=RGB_to_HSI(img)
  I=double(img)/255;
  R=I(:,:,1);
  G=I(:,:,2);
  B=I(:,:,3);
  
  num=1/2*((R-G)+(R-B));
  denom=((R-G).^2+((R-B).*(G-B))).^0.5;
  
  H=acosd((num./(denom+0.000001)));   # adding small value to avoid divide by zero error
  
  H(B>G)=360-H(B>G);
  H=H/360;
  
  S=1-(3./(sum(I,3)+0.000001)).*min(I,[],3);
  I=sum(I,3)./3;
  
  out=zeros(size(img));
  out(:,:,1)=H;
  out(:,:,2)=S;
  out(:,:,3)=I;
endfunction


function out=RGB_to_YCbCr(img)
  img_size=size(img);
  out=zeros(img_size);
  R=img(:,:,1);
  G=img(:,:,2);
  B=img(:,:,3);
  
  out(:,:,1) = 16 + 0.257*R+0.504*G+0.098*B;
  out(:,:,2) = 128 - 0.148*R-0.291*G+0.439*B;
  out(:,:,3) = 128 + 0.439*R-0.368*G-0.071*B;
endfunction


function out=XYZ_to_RGB(img)
  img_size=size(img);
  out=zeros(img_size);
  X=img(:,:,1);
  Y=img(:,:,2);
  Z=img(:,:,3);
  
  out(:,:,1) = 3.2404542*X - 1.5371385*Y - 0.4985314*Z;
  out(:,:,2) = - 0.9692660*X + 1.8760108*Y + 0.0415560*Z;
  out(:,:,3) = 0.0556434*X - 0.2040259*Y + 1.0572252*Z;
endfunction


function out=HSI_to_RGB(img)
  H1=img(:,:,1);
  S1=img(:,:,2);
  I1=img(:,:,3);
  H1=H1*360;
  
  R1=zeros(size(H1));
  G1=zeros(size(H1));
  B1=zeros(size(H1));
  RGB1=zeros([size(H1), 3]);
  
  B1(H1<120)=I1(H1<120).*(1-S1(H1<120));  
  R1(H1<120)=I1(H1<120).*(1+((S1(H1<120).*cosd(H1(H1<120)))./cosd(60-H1(H1<120))));  
  G1(H1<120)=3.*I1(H1<120)-(R1(H1<120)+B1(H1<120));
  
  H2=H1-120;  
  
  R1(H1>=120&H1<240)=I1(H1>=120&H1<240).*(1-S1(H1>=120&H1<240));  
  G1(H1>=120&H1<240)=I1(H1>=120&H1<240).*(1+((S1(H1>=120&H1<240).*cosd(H2(H1>=120&H1<240)))./cosd(60-H2(H1>=120&H1<240))));  
  B1(H1>=120&H1<240)=3.*I1(H1>=120&H1<240)-(R1(H1>=120&H1<240)+G1(H1>=120&H1<240));
  
  H2=H1-240;  
  
  G1(H1>=240&H1<=360)=I1(H1>=240&H1<=360).*(1-S1(H1>=240&H1<=360));  
  B1(H1>=240&H1<=360)=I1(H1>=240&H1<=360).*(1+((S1(H1>=240&H1<=360).*cosd(H2(H1>=240&H1<=360)))./cosd(60-H2(H1>=240&H1<=360))));  
  R1(H1>=240&H1<=360)=3.*I1(H1>=240&H1<=360)-(G1(H1>=240&H1<=360)+B1(H1>=240&H1<=360));
  
  RGB1(:,:,1)=R1;  
  RGB1(:,:,2)=G1;  
  RGB1(:,:,3)=B1;
  
  out=RGB1;
endfunction


function out=YCbCr_to_RGB(img)
  img_size=size(img);
  Y=img(:,:,1)-16;
  Cb=img(:,:,2)-128;
  Cr=img(:,:,3)-128;
  out=zeros(img_size);
  
  out(:,:,1)=1.164*Y+0*Cb+1.596*Cr;
  out(:,:,2)=1.164*Y-0.392*Cb-0.813*Cr;
  out(:,:,3)=1.164*Y+2.017*Cb+0*Cr;
endfunction

   
figure;
subplot(1,2,1);
ans=RGB_to_XYZ(img);
imshow(uint8(ans));

title('RGB to XYZ','fontsize',15) ;

subplot(1,2,2);
ans3=XYZ_to_RGB(ans);
imshow(uint8(ans3));
title('XYZ to RGB','fontsize',15) ;
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15)

figure;
subplot(1,2,1);
ans1=RGB_to_HSI(img);
imshow(ans1);
title("RGB to HSI", 'fontsize',15);
subplot(1,2,2);
ans4=HSI_to_RGB(ans1);
imshow(ans4);
title("HSI to RGB", 'fontsize',15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15)

figure;
subplot(1,2,1);
ans2=RGB_to_YCbCr(img);
imshow(uint8(ans2));
title("RGB to YCbCr",'fontsize',15);

subplot(1,2,2);
ans5=YCbCr_to_RGB(ans2);
imshow(uint8(ans5));
title("YCbCr to RGB", 'fontsize', 15);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15)