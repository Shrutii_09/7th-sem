clear;
clc;
pkg load image;

function out=average(img, mask_size)
  mask=ones(mask_size, mask_size)/(mask_size*mask_size);
  img_size=size(img)
  out=zeros(img_size(1)-mask_size+1, img_size(2)-mask_size+1,img_size(3));
    for i=1:img_size(1)-mask_size+1
      for j=1:img_size(2)-mask_size+1
        out(i,j,:)=sum(sum(img(i:i+mask_size-1, j:j+mask_size-1, :).*mask));
      endfor
    endfor
  out=uint8(out);
endfunction

img=imread("pepper.jpg");
figure;
subplot(2,2,1);
ans=average(img, 3);
imshow(ans)
title('3 x 3 mask','fontsize',15) ;

subplot(2,2,2);
ans=average(img, 5);
imshow(ans)
title('5 x 5 mask','fontsize',15) ;

subplot(2,2,3);
ans=average(img, 7);
imshow(ans)
title('7 x 7 mask','fontsize',15) ;

subplot(2,2,4);
ans=average(img, 9);
imshow(ans)
title('9 x 9 mask','fontsize',15) ;

S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);
