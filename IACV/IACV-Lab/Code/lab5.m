#LAB EXPT 5: Auto threshold computation

clc; clear all; close all;
pkg load io;
pkg load image;

cam = imread("Cameraman.jpg");

[hist, bins] = imhist(cam);     %Computing the histogram of input image

thresh = 128     %Defining the seed threshold

while 1     %Global thresholding
  g1 = (bins <= thresh);      %gray levels equal to or below threshold
  u1 = sum(bins(g1).*hist(g1))/sum(hist(g1));     %average value of grays level of pixels below the threshold
  
  g2 = (bins > thresh);     %gray levels above threshold
  u2 = sum(bins(g2).*hist(g2))/sum(hist(g2));     %average value of gray level of pixels above threshold
  
  new_thresh = (u1+u2)/2;      %computing new threshold
  
  if new_thresh == thresh     %replacing threshold with new value
    break;
  else
    thresh = new_thresh
  endif
  
endwhile

cam_thresh = zeros(size(cam));
cam_thresh(cam > thresh) = 255;
cam_thresh(cam <= thresh) = 0;

figure;
subplot(2,2,1);
imshow(cam)
title('Original image','fontsize',15) ;

subplot(2,2,2);
imhist(cam)
title('Histogram of input image', 'fontsize',15);

subplot(2,2,3) ;
imshow(cam_thresh) 
title(sprintf("Cameraman thresholded at %f", thresh),'fontsize',15);

subplot(2,2,4);
imhist(cam_thresh)
title('Histogram of thresholded image','fontsize',20);
S  = axes( 'visible', 'off', 'title', 'BT18ECE099 Shruti Murarka' , 'fontsize', 15);
