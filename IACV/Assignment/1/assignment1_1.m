clc;
clear;
close all;
warning off;

A = [956 107 490 411 489 154	52 762 113 443 859 323
476	895	714	253	767	515	731	40	415	984	676	471
953	441	136	844	179	705	702	947	156	521	67	902
152	334	745	850	247	223	122	953	698	54	338	337
285	132	702	217	422	111	834	474	283	822	633	421
141	935	855	297	618	236	38	371	516	11	303	957
468	681	872	15	642	692	49	947	912	181	281	93
816	847	640	996	59	414	345	779	106	128	385	165
813	399	231	292	388	511	659	623	686	933	828	470
702	546	337	77	300	916	904	132	795	670	625	943
231	112	385	93	939	948	907	251	837	781	871	674
636	112	410	599	67	298	998	278	789	142	354	734
300	164	59	787	373	20	837	841	3	223	577	573
231	585	528	179	717	114	823	458	386	995	258	784];

B = [376	475	954	97	854	744	432	7	908	829	343	763
848	230	707	147	733	757	477	196	163	286	614	623
283	308	146	266	779	582	27	213	227	717	906	130
424	346	14	688	183	775	281	201	683	40	699	576
72	959	781	727	533	777	977	105	480	677	620	587
34	514	622	109	997	143	666	524	259	532	121	804
667	30	333	168	724	794	116	679	686	298	615	483
135	201	622	587	809	745	940	925	270	392	930	367
29	303	462	531	405	430	884	49	964	937	936	110
21	776	224	55	686	217	786	195	252	63	815	3
0	584	631	944	164	931	38	317	328	747	483	64
672	393	57	847	512	132	115	994	865	8	667	364
454	648	426	195	918	185	736	734	534	486	865	336
56	717	129	451	986	929	739	790	830	820	524	431];

P= [956	107	490	411	489
476	895	714	253	767
953	441	136	844	179
152	334	745	850	247
285	132	702	217	422];

%disp("Transpose of Matrix A is:");
%tic(); disp(A'); toc()

%disp("Transpose of Matrix B is:");
%tic(); disp(B'); toc()

%disp("Transpose of Matrix P is:");
%tic(); disp(P'); toc

%disp("Inverse of Matrix P is:");
%tic(); disp(inv(P)); toc()

%disp("A+B");
%tic(); disp(A+B); toc()

%disp("A-B");
%tic(); disp(A-B); toc()

%disp("Matrix multiplication A*B'");
%tic
%C=A*(B')
%toc

%disp(C/1000000)

%disp("A*9");
%tic(); disp(A*9); toc()

%disp("B*9");
%tic(); disp(B*9); toc()

%disp("P*9");
%tic(); disp(P*9); toc()

%disp("A/2");
%tic(); disp(A/2); toc()

%disp("B/2");
%tic(); disp(B/2); toc()

%disp("P/2");
%tic(); disp(P/2); toc()

%disp("Element wise - A*B");
%tic(); disp(A.*B); toc()
tic
a = [];  %matrix to store positions where X=3 apears in A
b = []; %matrix to store positions where X=3 apears in B
p = [];  %matrix to store positions where X=3 apears in P
X = 3
for i = 1:14
  for j = 1:12
    if A(i,j) == X
      a = [a;i,j];
    endif
    if B(i,j) == X
      b = [b;i,j];
    endif
    if i <6 & j<6
      if P(i,j) == X
        p = [p;i,j];
      endif
    endif
    
  end
end
toc

disp("3 in A");
disp(a)

disp("3 in B");
disp(b)
disp("3 in P");
disp(p)

tic;
X = 3;
A_amplified = A; %just to keep A unchanged. element of A_amplified will be multiplied by 2
B_amplified = B; 
P_amplified = P; 
a = 0;b=0;p=0;
for i = 1:14
    for j = 1:12
      
    if a==0 || b==0 || p==0
      if A(i,j) == X 
        A_amplified(i,j) = 2*A_amplified(i,j);
        a = 1;
      endif
      if B(i,j) == X 
        B_amplified(i,j) = 2*B_amplified(i,j);
        b = 1;
      endif
      if i <6 & j<6
        if P(i,j) == X 
          P_amplified(i,j) = 2*p_amplified(i,j);
          p = 1;
        endif
      endif
     else
      break
      endif
    endfor
endfor
toc;
disp("A Amplified")
disp(A_amplified)
disp("B Amplified")
disp(B_amplified)
disp("P Amplified")
disp(P_amplified)

tic;
X = 953;
A_new = A; %just to keep A unchanged. element of A_new will be replaced by 31.
B_new = B; 
P_new = P; 
occ_in_A = 0;
occ_in_B = 0;
occ_in_P = 0;
for i = 1:14
    for j = 1:12
      if A(i,j) == X 
        A_new(i,j) = 31;
        occ_in_A  =occ_in_A +1;
      endif
      if B(i,j) == X 
        B_new(i,j) = 31;
        occ_in_B= occ_in_B+1;
      endif
      if i <6 & j<6
        if P(i,j) == X 
          P_new(i,j) = 31;
          occ_in_P =occ_in_P+1;
        endif
      endif
   
    endfor
endfor
toc;
disp(occ_in_A)
disp(occ_in_B)
disp(occ_in_P)
disp("A New")
disp(A_new)
disp("B New")
disp(B_new)
disp("P New")
disp(P_new)