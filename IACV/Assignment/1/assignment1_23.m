clc;
clear;
close all;
pkg load image;
I = imread("Lena.jpg");
J = imread("Cameraman.jpg");
tic
d = 1;
for x = 1:3
I_scalar = I;
J_scalar = J;
e1 = 0.3*x+2; e2 = 0.3*x-2;
for i = 1:512 
  for j = 1:512
  if I_scalar(i,j) > 127
    I_scalar(i,j) = e1*I_scalar(i,j); 
    elseif I_scalar(i,j) < 128
      I_scalar(i,j) = e2*I_scalar(i,j); 
   end
  if J_scalar(i,j) > 127
    J_scalar(i,j) = e1*J_scalar(i,j); 
   elseif J_scalar(i,j) < 128
    J_scalar(i,j) = e2*J_scalar(i,j);
    end
end 
end
figure(d);
imshow(I_scalar); d = d+1; 
figure(d);
imshow(J_scalar); d = d+1;
end
toc
