##clc
##clear all
##close all
##warning off;
##pkg load image;
##
##function [y] = threshold(x,thresh)
##[ m,n] = size(x);
##y = zeros(m,n);
##for i = 1:m 		%iterating over the rows
##    for j = 1:n     %iterating over the columns
##      if x(i,j)>thresh
##        y(i,j)= 255;
##      else
##        y(i,j) = 0;
##      endif
##  endfor
##endfor
##end
##
##lena = imread('Lena.jpg');
##cm = imread('Cameraman.jpg');
##subplot(2,2,1);
##imshow(lena);
##title('original Lena img');
##subplot(2,2,2);
##imshow(cm);
##title('original Cameraman Image');
##thresh_a = input("enter the value");
##lena = threshold(lena, thresh_a);
##cm = threshold(cm, thresh_a);
##
##subplot(2,2,3);
##imshow(lena)
##title('lena after thresholding');
##subplot(2,2,4);
##imshow(cm);
##title('cameraman after thresholding');

clc
clear all
close all
warning off;
pkg load image;

function [y] = local_threshold(x,thresh)
[m, n] = size(x);
y = x;
for i= 100:412
  for j=100:212
      if x(i,j)>thresh
        y(i,j)= 255;
      else
        y(i,j) = 0;
      endif
  endfor
endfor
end
lena = imread('Lena.jpg');
cm = imread('Cameraman.jpg');
lena = lena - 151;
cm = cm - 151;
subplot(3,2,1);
imshow(lena);
title('Original Lena img',"fontsize",20);
subplot(3,2,2);
imshow(cm);
title('Original Cameraman Image',"fontsize",20);
a = min(min(lena));
b = max(max(lena));
thresh_lena = (a+b)/2;
c = min(min(cm));
d  =max(max(cm));
thresh_cm = (c+d)/2;
lena_b = local_threshold(lena, thresh_lena);
cm_b = local_threshold(cm, thresh_lena);
subplot(3,2,3);
imshow(lena_b);
title('Max-Min Thresholded Lena img',"fontsize",20);
subplot(3,2,4);
imshow(cm_b);
title('Max-Min Thresholded Cameraman Image',"fontsize",20);

sum1 = sum(sum(lena));
sum2 = sum(sum(cm));
sum1 = sum1/(512*512);
sum2 = sum2/(512*512);
lena_c = local_threshold(lena, sum1);
cm_c = local_threshold(cm, sum2);

subplot(3,2,5);
imshow(lena_c);
title('Avg Thresholded Lena img',"fontsize",20);
subplot(3,2,6);
imshow(cm_c);
title('Avg Thresholded Cameraman Image',"fontsize",20);

