clc;
close all;

%Function for Threshold 1
function y = threshold1(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 127
            y(i,j) = 0;
        else
            y(i,j) = 255;
        endif
    endfor
endfor
end


%Function for Threshold 2
function y = threshold2(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 127
            y(i,j) = 255;
        else
            y(i,j) = 0;
        endif
    endfor
endfor
end

%Function for Threshold 3
function y = threshold3(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 127
            y(i,j) = 0;
        else
            y(i,j) =  fix((255/128) *x(i,j) - 253 );
        endif
    endfor
endfor
end

%Function for Threshold 4
function y = threshold4(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 127
            y(i,j) =  fix(255-(255/127)*x(i,j));;
        else
            y(i,j) =  0;
        endif
    endfor
endfor
end

%Function for Threshold 5
function y = threshold5(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 127
            y(i,j) = 0;
        elseif x(i,j) <=200
            y(i,j) = 127 ;
        else
            y(i,j) = 255;
        endif
    endfor
endfor
end

%Function for Threshold 6
function y = threshold6(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 100
            y(i,j) = 0;
        elseif x(i,j) <= 175
            y(i,j) = 175 ;
        else
            y(i,j) = 255;
        endif
    endfor
endfor
end

%Function for Threshold 7
function y = threshold7(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 100
            y(i,j) = x(i,j);
        elseif x(i,j) <= 175
            y(i,j) = 175 ;
        else
            y(i,j) = x(i,j) ;
        endif
    endfor
endfor
end

%Function for Threshold 8
function y = threshold8(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 75
            y(i,j) = 0;
        elseif x(i,j) <= 100
            y(i,j) = fix((75/25)*x(i,j) - 225) ;
        elseif x(i,j) <= 127
            y(i,j) = 100;
        elseif  x(i,j) < 200
            y(i,j) =  fix((27/73)*x(i,j) + 53.02);
        elseif x(i,j) == 200
            y(i,j) =200;
        else
            y(i,j) = x(i,j);
        endif
    endfor
endfor
end

%Function for Threshold 9
function y = threshold9(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 100
            y(i,j) = fix(255-((4/5)*x(i,j)));
        elseif x(i,j) <= 175
            y(i,j) = 100;
        else
            y(i,j) = fix(318.75 - ((5/4)*x(i,j)));
        endif
    endfor
endfor
end

%Function for Threshold 10
function y = threshold10(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 100
            y(i,j) = 255;
        elseif x(i,j) <= 175
            y(i,j) = fix(275 - x(i,j));
        else
            y(i,j) = 100;
        endif
    endfor
endfor
end

%Function for Threshold 11
function y = threshold11(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 100
            y(i,j) = 255;
        elseif x(i,j) < 200
            y(i,j) = fix(273 - ((73/100)*x(i,j)));
        elseif x(i,j) == 200
            y(i,j ) = 100;
        else
            y(i,j) = fix(463.63 - ((100/55)*x(i,j)));
        endif
    endfor
endfor
end

%Function for Threshold 12
function y = threshold12(x)
[m,n] = size(x);
y = zeros(m,n);
for i = 1:m 		%iterating over the rows
    for j = 1:n     %iterating over the columns
        if x(i,j) <= 75
            y(i,j) = 0;
        elseif x(i,j) <= 100
            y(i,j) = 75;
        elseif x(i,j) == 125
            y(i,j ) = 100;
        elseif x(i,j) == 150
            y(i,j ) = 125;
        elseif x(i,j) == 175
            y(i,j ) = 150;
        elseif x(i,j) == 200
            y(i,j ) = 210;
        else
            y(i,j) = 255;
        endif
    endfor
endfor
end
 
%Taking Input
img_cam = imread('Cameraman.jpg');
img_lena = imread('Lena.jpg');

%Applying Threshold
tic
img_cam_t1 = uint8(threshold1(img_cam));
img_lena_t1 = uint8(threshold1(img_lena));

img_cam_t2 = uint8(threshold2(img_cam));
img_lena_t2 = uint8(threshold2(img_lena));

img_cam_t3 = uint8(threshold3(img_cam));
img_lena_t3 = uint8(threshold3(img_lena));

img_cam_t4 = uint8(threshold4(img_cam));
img_lena_t4 = uint8(threshold4(img_lena));

img_cam_t5 = uint8(threshold5(img_cam));
img_lena_t5 = uint8(threshold5(img_lena));

img_cam_t6 = uint8(threshold6(img_cam));
img_lena_t6 = uint8(threshold6(img_lena));

img_cam_t7 = uint8(threshold7(img_cam));
img_lena_t7 = uint8(threshold7(img_lena));

img_cam_t8 = uint8(threshold8(img_cam));
img_lena_t8 = uint8(threshold8(img_lena));

img_cam_t9 = uint8(threshold9(img_cam));
img_lena_t9 = uint8(threshold9(img_lena));

img_cam_t10 = uint8(threshold10(img_cam));
img_lena_t10 = uint8(threshold10(img_lena));

img_cam_t11 = uint8(threshold11(img_cam));
img_lena_t11 = uint8(threshold11(img_lena));

img_cam_t12 = uint8(threshold12(img_cam));
img_lena_t12 = uint8(threshold12(img_lena));
toc
%Saving results
imwrite(img_cam_t1,'cam_t1.jpg');
imwrite(img_lena_t1,'lena_t1.jpg');

imwrite(img_cam_t2,'cam_t2.jpg');
imwrite(img_lena_t2,'lena_t2.jpg');

imwrite(img_cam_t3,'cam_t3.jpg');
imwrite(img_lena_t3,'lena_t3.jpg');

imwrite(img_cam_t4,'cam_t4.jpg');
imwrite(img_lena_t4,'lena_t4.jpg');

imwrite(img_cam_t5,'cam_t5.jpg');
imwrite(img_lena_t5,'lena_t5.jpg');

imwrite(img_cam_t6,'cam_t6.jpg');
imwrite(img_lena_t6,'lena_t6.jpg');

imwrite(img_cam_t7,'cam_t7.jpg');
imwrite(img_lena_t7,'lena_t7.jpg');

imwrite(img_cam_t8,'cam_t8.jpg');
imwrite(img_lena_t8,'lena_t8.jpg');

imwrite(img_cam_t9,'cam_t9.jpg');
imwrite(img_lena_t9,'lena_t9.jpg');

imwrite(img_cam_t10,'cam_t10.jpg');
imwrite(img_lena_t10,'lena_t10.jpg');

imwrite(img_cam_t11,'cam_t11.jpg');
imwrite(img_lena_t11,'lena_t11.jpg');

imwrite(img_cam_t12,'cam_t12.jpg');
imwrite(img_lena_t12,'lena_t12.jpg');